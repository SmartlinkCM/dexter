#!/bin/bash
# chmod u+x compile.sh

# last compiled with
# ligo --version
# Commit SHA: 0ca1331d30faffcc5e1f450541daf76d22f1f990
# Commit Date: 2020-11-11 23:19:32 +0000


# this script depends on:
# - ligo
# - dos2unix

ligo compile-contract ./dexter-fa1.2.ligo main | dos2unix > contracts/dexter.tz
ligo compile-contract --michelson-format=json ./dexter-fa1.2.ligo main | dos2unix > contracts/dexter.json

ligo compile-contract ./dexter-fa2.ligo main | dos2unix > contracts/dexter-fa2.tz
ligo compile-contract --michelson-format=json ./dexter-fa2.ligo main | dos2unix > contracts/dexter-fa2.json

ligo compile-contract ./attack.ligo main | dos2unix > contracts/attack.tz
