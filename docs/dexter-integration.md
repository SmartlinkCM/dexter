How to integrate a Dexter contract into your application.

To support a Dexter exchange in your application will need to collect
the following data a Dexter conntract:

- The FA1.2 token address.
- The big map id of token address.
- The dexter address that points to the token address.
- The big map id of the dexter address.

# FA1.2 storage

```
(pair (big_map %accounts address (pair (map %allowances address nat) (nat %balance)))
      (nat %total_supply))
```

- `accounts` a mapping of how much an address owns and allowances that allow other contracts to spend the owners token
- `total_supply` the total amount of token that exists. This is also equal to the sum of all account balances.

# Dexter storage

```
(pair (big_map %accounts address (pair (map %allowances address nat) (nat %balance)))
      (pair %s
         (pair (pair (bool %freeze_baker) (nat %lqt_total))
               (pair (address %manager) (address %token_address)))
         (nat %token_pool)))
```

- `accounts`
- `freeze_baker`
- `lqt_total`
- `manager`
- `token_address`
- `token_pool`

# Dexter Entry Points

## addLiquidity

The user will provide you with an XTZ amount and/or an FA1.2 amount.
Given an XTZ amount they want to deposit you can calculate how much
FA1.2 they need to provide and vice versa. You can also calculate how
much liquidity (LQT) will be minted.

- the `lqt_total` from dexter storage.
- the `token_pool` from dexter storage.
- the xtz `balance` from dexter.
- the `account` address that is depositing liquidity in dexter.
- the amount of xtz the user wants to deposit, `xtz_deposit`.

Given the amount of xtz the user wants to deposit, we can calculate the amount 
of FA1.2 that would be required (tokens_deposited).

```
if (lqt_total > 0) {
  // this means there is no liquidity

  xtz_pool = user input of xtz

  tokens_deposited = (xtz_deposited * token_pool) / xtz_pool

  lqt_minted = xtz_deposited * lqt_total / xtz_pool

} else {
  tokens_deposited = get user input fa1.2, they decide first exchange rate
  
  lqt_minted = balance
}
```

You will also need to pick a deadline for when the transaction sure occur latest 
(this is the case for most of the Dexter entry points).

```
deadline = now + amount_of_time
```

## tezos client example

```bash
tezos-client transfer 10 from alice to tezosGoldExchange --entrypoint 'addLiquidity' --arg 'Pair (Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" 1) (Pair 9000 "2030-01-01T12:00:00Z")' --burn-cap 1
```

### with string interpolation

- `xtz_deposited` tez
- `owner` address
- `min_liquidity_created` nat
- `max_tokens_deposited` nat
- `deadline` timestamp

```bash
tezos-client transfer $xtz_deposited from alice to tezosGoldExchange --entrypoint 'addLiquidity' --arg 'Pair (Pair "$owner" $min_liquidity_created) (Pair $max_tokens_deposited "$deadline")' --burn-cap 1
```

# removeLiquidity

Give an amount of liquidity that the user wants to remove we can calculate
the amount of XTZ and FA1.2 that will be returned. You need to the following
data:

- `lqt_burned`: the number of LQT tokens the user wants to remove.
- `xtz_pool`: the `xtz` balance from dexter
- `token_pool`: from dexter storage.
- `lqt_total`: from dexter storage.

```
xtz_withdrawn    = lqt_burned * xtz_pool   / lqt_total

tokens_withdrawn = lqt_burned * token_pool / lqt_total
```

## tezos client example

```bash
tezos-client transfer 0 from alice to tezosGoldExchange --entrypoint 'removeLiquidity' --arg 'Pair (Pair (Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV") (Pair 5000000 1)) (Pair 1 "2020-06-29T18:00:21Z")' --burn-cap 1
```

### with string interpolation

- `owner` address
- `to` address
- `lqt_burned` nat
- `min_xtz_withdrawn` tez
- `min_token_withdrawn` nat
- `deadline` timestamp

```bash
tezos-client transfer 0 from alice to tezosGoldExchange --entrypoint 'removeLiquidity' --arg 'Pair (Pair (Pair "$owner" "$to") (Pair $lqt_burned $min_xtz_withdrawn)) (Pair $min_token_withdrawn "$deadline")' --burn-cap 1
```

# xtzToToken

Given an amount of XTZ the user wants to sell, we can caculate how much token
they will receive, and vice-versa. There is a 0.3% fee taken from the XTZ amount
sold and the amount sold is added to the denominator (xtz_pool) to increase the
slippage of larger exchangnes. You need to know the following.

- `xtz_sold`: amount of XTZ the user is selling
- `token_pool`: from the dexter storage
- `xtz_pool`: balance from dexter contract

```
tokens_bought = (xtz_sold * token_pool * 0.997) / (xtz_pool + xtz_sold * 0.997);
```

If you want to calculate how much XTZ sold to get a certain amount of tokens.

- `tokens_bought`: amount of tokens the user wants to buy

```
xtz_sold = (xtz_pool * tokens_bought * 0.997) / (token_pool + tokens_bought * 0.997);
```

## tezos client example

```bash
tezos-client transfer 2 from bob to tezosGoldExchange --entrypoint 'xtzToToken' --arg 'Pair (Pair "tz1MQehPikysuVYN5hTiKTnrsFidAww7rv3z" 1) "2020-06-29T18:00:21Z"' --burn-cap 1
```

### with string interpolation

- `xtz_sold` 
- `to` address
- `min_tokens_bought` nat (for safety calculate how much you should receive based on xtz_sold)
- `deadline` timestamp

```
tezos-client transfer $xtz_sold from bob to tezosGoldExchange --entrypoint 'xtzToToken' --arg 'Pair (Pair "$to" $min_tokens_bought) "$deadline"' --burn-cap 1
```

# tokenToXtz

You need to give Dexter permission to sell any particular amount of FA1.2 token.

Given an amount of FA1.2 the user wants to sell, we can caculate how much XTZ
they will receive, and vice-versa. There is a 0.3% fee taken from the FA1.2 amount
sold and the amount sold is added to the denominator (token_pool) to increase the
slippage of larger exchangnes. You need to know the following.

- `token_sold`: amount of FA1.2 the user is selling
- `token_pool`: from the dexter storage
- `xtz_pool`: balance from dexter contract

```
xtz_bought = (tokens_sold * 0.997 * xtz_pool) / token_pool + (tokens_sold * 0.997);
```

If you want to calculate how much FA1.2 sold to get a certain amount of tokens.

- `xtz_bought`: amount of XTZ the user wants to buy.

```
tokens_sold = (xtz_bought * token_pool * 0.997) / (xtz_pool + xtz_bought * 0.997);
```

## tezos client example

```bash
tezos-client transfer 0 from simon to tezosGoldExchange --entrypoint 'tokenToXtz' --arg 'Pair (Pair (Pair "tz1QGsyN9TyFHJbKTcF3FtKbm5Gye3QqXw3t" "tz1QGsyN9TyFHJbKTcF3FtKbm5Gye3QqXw3t") (Pair 40 1)) "2020-06-29T18:00:21Z"' --burn-cap 1
```

### with string interpolation

- `xtz_sold` 
- `to` address
- `min_tokens_bought` nat (for safety calculate how much you should receive based on xtz_sold)
- `deadline` timestamp

```
tezos-client transfer 0 from simon to tezosGoldExchange --entrypoint 'tokenToXtz' --arg 'Pair (Pair (Pair "$owner" "$to") (Pair $tokens_sold $min_xtz_bought)) "$deadline"' --burn-cap 1
```

## approval

Allow a third party to perform remove liquidity for an a liquidity owner.

To approve an allowance for a third party you need to know the current allowance that 
the third party has. This can be acquired from the big store of the dexter contract.
This is done for security reasons.

## tezos client example

```bash
tezos-client transfer 0 from simon to tezosGoldExchange --entrypoint 'approve' --arg 'Pair (Pair "tz1QGsyN9TyFHJbKTcF3FtKbm5Gye3QqXw3t" 100) 100 "2020-06-29T18:00:21Z"' --burn-cap 1
```

### with string interpolation

- `xtz_sold` 
- `$updatedAllowance` nat
- `$currentAllowance` nat

```
tezos-client transfer 0 from simon to tezosGoldExchange --entrypoint 'approve' --arg 'Pair (Pair "$owner" $updatedAllownance) $currentAllowance "2020-06-29T18:00:21Z"' --burn-cap 1
```
