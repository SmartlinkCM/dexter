# Dexter Contract

The Dexter contract 


storage

```
storage (pair (big_map %accounts (address :owner)
                                 (pair (nat :balance)
                                       (map (address :spender)
                                            (nat :allowance))))
              (pair (pair (bool :selfIsUpdatingTokenPool)
                          (pair (bool :freezeBaker)
                                (nat :lqtTotal)))
                    (pair (pair (address :manager)
                                (address :tokenAddress))
                          (pair (nat :tokenPool)
                                (mutez :xtzPool)))));
```

entrypoints

approve

```
pair (address :spender) (pair (nat :allowance) (nat :currentAllowance))
```

addLiquidity

```
pair (pair (address :owner) (nat :minLqtMinted)) (pair (nat :maxTokensDeposited) (timestamp :deadline))
```

removeLiquidity

```
pair (pair (address :owner) (pair (address :to) (nat :lqtBurned)))
     (pair (mutez :minXtzWithdrawn) (pair (nat :minTokensWithdrawn) (timestamp :deadline)))
```

xtzToToken

```
```

tokenToXtz

```
```

tokenToToken

```
```

updateTokenPool

```
```

updateTokenPoolInternal

```
```

setBaker

```
```

setManager

```
```

default

```
```


                  (or 
                      (or (pair %xtzToToken (address :to)
                                            (pair (nat :minTokensBought)
                                                  (timestamp :deadline)))
                          (pair %tokenToXtz (pair (address :owner)
                                                  (address :to))
                                            (pair (nat :tokensSold)
                                                  (pair (mutez :minXtzBought)
                                                        (timestamp :deadline)))))))
              (or (or (pair %tokenToToken (pair (address :outputDexterContract)
                                                (pair (nat :minTokensBought)
                                                      (address :owner)))
                                          (pair (address :to)
                                                (pair (nat :tokensSold)
                                                      (timestamp :deadline))))
                      (or (key_hash %updateTokenPool)
                          (nat %updateTokenPoolInternal)))
                  (or (pair %setBaker (option key_hash)
                                      bool)
                      (or (address %setManager)
                          (unit %default)))))
