# Outline of Dexter Presentation 2020-03-06

[Tezos Developers Day, Paris, March 6th, 2020 at 14:45](https://nomadic-labs.com/tezosdevday.html)

Title: 'Dexter: a Decentralized Exchange for Tezos'
Duration: 45 minutes

- Dexter
  - What is Dexter
    - automatic market for Tezos and tokens
      - No centralized authority
      - how is the exchange rate decided prices decided
      - tezbridge
    - contract and frontend
  - Why are we building Dexter
- Ecosystem: What token standards are there on Tezos?
  - FA1.2
    - link the TZIP
    - briefly go over the entrypoints
  - FA2
    - keep it brief because we are not currently supporting it
- What are the features of Dexter
  - Add liquidity
  - Remove liquidity
  - Swap XTZ to Token (FA1.2)
  - Swap Token (FA1.2) to XTZ
  - Bid system
- Dexter Storage
- Explain Add liquidity
- Explain Remove liquidity
- Explain Swap XTZ to Token (FA1.2)
- Explain Swap Token (FA1.2) to XTZ
- Explain Bid System
- Current state of Dexter
  - quick demo of dexter
  - finalize the specification
  - formal proof
  - unit tests
  - support beacon
  - resolve baking issues (discuss more at the end)
- Possible Plans for Dexter v2
  - Support non-FA1.2 tokens
  - different market models
- Mention various groups we have worked with
  - Arvid
  - Beacon
  - Arthur
  - link some discussions from Agora
  - Ligo lang tool
- Open questions
  - Issues with baking
  - Incentive liquidity provides to not loot the rewards

