{-|
Module      : Main
Copyright   : (c) camlCase, 2019-2021
Maintainer  : james@camlcase.io

-}

module Main where

import qualified Dexter.Contract            as Dexter

import Test.Dexter.Contract.FA1_2.AddLiquidity    as FA1_2.AddLiquidity 
import Test.Dexter.Contract.FA1_2.Approve         as FA1_2.Approve
import Test.Dexter.Contract.FA1_2.Core            as FA1_2.Core
import Test.Dexter.Contract.FA1_2.Default         as FA1_2.Default
import Test.Dexter.Contract.FA1_2.RemoveLiquidity as FA1_2.RemoveLiquidity
import Test.Dexter.Contract.FA1_2.SetBaker        as FA1_2.SetBaker
import Test.Dexter.Contract.FA1_2.SetManager      as FA1_2.SetManager
import Test.Dexter.Contract.FA1_2.TokenToXtz      as FA1_2.TokenToXtz
import Test.Dexter.Contract.FA1_2.UpdateTokenPool as FA1_2.UpdateTokenPool
import Test.Dexter.Contract.FA1_2.UpdateTokenPoolInternal as FA1_2.UpdateTokenPoolInternal
import Test.Dexter.Contract.FA1_2.XtzToToken      as FA1_2.XtzToToken

import Test.Dexter.Contract.FA2.AddLiquidity    as FA2.AddLiquidity
import Test.Dexter.Contract.FA2.Approve         as FA2.Approve
import Test.Dexter.Contract.FA2.Core            as FA2.Core
import Test.Dexter.Contract.FA2.Default         as FA2.Default
import Test.Dexter.Contract.FA2.RemoveLiquidity as FA2.RemoveLiquidity
import Test.Dexter.Contract.FA2.SetBaker        as FA2.SetBaker
import Test.Dexter.Contract.FA2.SetManager      as FA2.SetManager
import Test.Dexter.Contract.FA2.TokenToXtz      as FA2.TokenToXtz
import Test.Dexter.Contract.FA2.UpdateTokenPool as FA2.UpdateTokenPool
import Test.Dexter.Contract.FA2.UpdateTokenPoolInternal as FA2.UpdateTokenPoolInternal
import Test.Dexter.Contract.FA2.XtzToToken      as FA2.XtzToToken

import Test.Hspec                            (hspec, parallel)

main :: IO ()
main = do
  case (Dexter.defaultMutezValues, Dexter.defaultErrorMessages) of
    (Just mutezValues, Right errorMessages) -> do            
      let runtimeValues =
            Dexter.RuntimeValues mutezValues errorMessages

      hspec $ parallel $ do
        FA1_2.AddLiquidity.spec runtimeValues
        FA1_2.Approve.spec runtimeValues
        FA1_2.Core.spec
        FA1_2.Default.spec runtimeValues
        FA1_2.RemoveLiquidity.spec runtimeValues
        FA1_2.SetBaker.spec runtimeValues
        FA1_2.SetManager.spec runtimeValues
        FA1_2.TokenToXtz.spec runtimeValues
        FA1_2.UpdateTokenPool.spec runtimeValues
        FA1_2.UpdateTokenPoolInternal.spec runtimeValues
        FA1_2.XtzToToken.spec runtimeValues

      hspec $ parallel $ do
        FA2.AddLiquidity.spec runtimeValues
        FA2.Approve.spec runtimeValues
        FA2.Core.spec
        FA2.Default.spec runtimeValues
        FA2.RemoveLiquidity.spec runtimeValues
        FA2.SetBaker.spec runtimeValues
        FA2.SetManager.spec runtimeValues
        FA2.TokenToXtz.spec runtimeValues
        FA2.UpdateTokenPool.spec runtimeValues
        FA2.UpdateTokenPoolInternal.spec runtimeValues
        FA2.XtzToToken.spec runtimeValues
      
    _ -> fail "Unable to make runtime values"
