{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications  #-}

module Main where

-- base
import qualified Data.Map as Map
import           Prelude hiding (one)

-- lorentz
import           Lorentz
import           Lorentz.ContractRegistry

-- dexter
import qualified Dexter.Contract as Dexter

main :: IO ()
main = do
  case (Dexter.defaultMutezValues, Dexter.defaultErrorMessages) of
    (Just mutezValues, Right errorMessages) -> do            
      let runtimeValues =
            Dexter.RuntimeValues
            mutezValues
            errorMessages

      let contractRegistry =
            ContractRegistry $ Map.fromList
              [ "Dexter-FA1.2" ?:: ContractInfo
                { ciContract      = (Dexter.contract runtimeValues :: Contract Dexter.ParameterFA1_2 Dexter.StorageFA1_2)
                , ciIsDocumented  = True
                , ciStorageParser = Nothing
                , ciStorageNotes  = Just (Dexter.storageNotes @Dexter.StorageFA1_2)
                },
                "Dexter-FA2" ?:: ContractInfo
                { ciContract      = (Dexter.contract runtimeValues :: Contract Dexter.ParameterFA2 Dexter.StorageFA2)
                , ciIsDocumented  = True
                , ciStorageParser = Nothing
                , ciStorageNotes  = Just (Dexter.storageNotes @Dexter.StorageFA2)
                }
              ]

      -- Print nameOfContract mOutputFile forceOneLine useMicheline
      runContractRegistry contractRegistry (Print "Dexter-FA1.2" (Just "dexter.tz") False False)
      runContractRegistry contractRegistry (Print "Dexter-FA2" (Just "dexter-fa2.tz") False False)
      -- tezos-client typecheck script dexter.tz
      -- tezos-client typecheck script dexter-fa2.tz

    _ -> fail "Unable to make runtime values"

