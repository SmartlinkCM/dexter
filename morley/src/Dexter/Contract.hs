{-|
Module      : Dexter.Contract
Copyright   : (c) camlCase, 2020

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DeriveAnyClass            #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE FunctionalDependencies    #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Dexter.Contract
  ( module Dexter.Contract
  , module Dexter.Contract.Core    
  , module Dexter.Contract.Approve
  , module Dexter.Contract.AddLiquidity
  , module Dexter.Contract.RemoveLiquidity
  , module Dexter.Contract.TokenToXtz
  , module Dexter.Contract.XtzToToken
  , module Dexter.Contract.UpdateTokenPool    
  ) where

import Data.Typeable (Typeable)

import Dexter.Contract.Approve
import Dexter.Contract.AddLiquidity
import Dexter.Contract.RemoveLiquidity
import Dexter.Contract.TokenToXtz
import Dexter.Contract.XtzToToken
import Dexter.Contract.UpdateTokenPool

import Dexter.Contract.Core

import Lorentz

-- =============================================================================
-- Contract Code
-- =============================================================================

class MkContractCode param storage | param -> storage, storage -> param where
  mkContractCode :: (forall s1. RuntimeValues s1) -> ContractCode param storage

instance MkContractCode ParameterFA1_2 StorageFA1_2 where
  mkContractCode runtimeValues = do
    unpair
    entryCaseSimple @ParameterFA1_2
      ( #cApprove         /-> approve         runtimeValues
      , #cAddLiquidity    /-> addLiquidity    runtimeValues
      , #cRemoveLiquidity /-> removeLiquidity runtimeValues
      , #cXtzToToken      /-> xtzToToken      runtimeValues
      , #cTokenToXtz      /-> tokenToXtz      runtimeValues
      , #cUpdateTokenPool /-> updateTokenPool runtimeValues
      , #cUpdateTokenPoolInternal /-> updateTokenPoolInternal runtimeValues
      , #cSetBaker        /-> setBaker        runtimeValues
      , #cSetManager      /-> setManager      runtimeValues
      , #cDefault         /-> default_        runtimeValues
      )

instance MkContractCode ParameterFA2 StorageFA2 where
  mkContractCode runtimeValues = do
    unpair
    entryCaseSimple @ParameterFA2
      ( #cApprove         /-> approve         runtimeValues
      , #cAddLiquidity    /-> addLiquidity    runtimeValues
      , #cRemoveLiquidity /-> removeLiquidity runtimeValues
      , #cXtzToToken      /-> xtzToToken      runtimeValues
      , #cTokenToXtz      /-> tokenToXtz      runtimeValues
      , #cUpdateTokenPool /-> updateTokenPool runtimeValues
      , #cUpdateTokenPoolInternal /-> updateTokenPoolInternal runtimeValues
      , #cSetBaker        /-> setBaker        runtimeValues
      , #cSetManager      /-> setManager      runtimeValues
      , #cDefault         /-> default_        runtimeValues
      )

contract
  :: forall updateTokenPoolInternalParams tokenAddress param storage.
     ( IsoValue tokenAddress
     , Typeable tokenAddress
     , IsoValue updateTokenPoolInternalParams
     , Typeable updateTokenPoolInternalParams
     , HasAnnotation updateTokenPoolInternalParams
     , TypeHasDoc updateTokenPoolInternalParams     
     , Parameter updateTokenPoolInternalParams ~ param
     , StorageSkeleton tokenAddress ~ storage
     , AssertSenderIsTokenAddress updateTokenPoolInternalParams storage
     , CallUpdateTokenPoolInternal param storage
     , ContractCallingXtzToToken param storage
     , SelfCallingDefault param storage
     , SetFieldTokenPool updateTokenPoolInternalParams storage
     , TransferDexterTokensTo param storage
     , TransferDexterTokensToReceiver param storage
     , TransferOwnerTokensToDexter param storage
     , MkContractCode param storage
     )
  => (forall s1. RuntimeValues s1)
  -> Contract param storage
contract runtimeValues =
  defaultContract $ contractName "Dexter" $ do
    contractGeneralDefault
    -- docStorage @StorageFA1_2
    -- doc $ DDescription contractDoc
    mkContractCode runtimeValues

-- | The contract manager can set the baker as long as freezeBaker is False.
-- This is the current solution until the virtual baker is available on Tezos.
-- When it is, the manager should set freezeBaker to True so they no longer have
-- control over who the baker is.
setBaker
  :: forall tokenAddress. (IsoValue tokenAddress)
  => (forall s1. RuntimeValues s1)
  -> Entrypoint SetBakerParams (StorageSkeleton tokenAddress)
setBaker runtimeValues = do
  -- assert selfIsUpdatingTokenPool == False
  dip (failIfSelfIsUpdatingTokenPool runtimeValues)
  -- assert amount == 0
  failIfAmountIsNotZero runtimeValues
  
  -- assert that the sender is the manager
  duupX @2; stToField #manager; sender; assertEq (senderIsNotContractManager . errorMessages $ runtimeValues)
  stackType @[SetBakerParams, (StorageSkeleton tokenAddress)]

  -- assert that changing the baker is not frozen
  duupX @2; stToField #freezeBaker; not; assert (bakerIsFrozen . errorMessages $ runtimeValues)
  stackType @[SetBakerParams, (StorageSkeleton tokenAddress)]

  -- set the delegate
  unpair; setDelegate
  stackType @[Operation, Bool, (StorageSkeleton tokenAddress)]
  
  -- set the freezeBaker value
  dip (do stSetField #freezeBaker; nil)
  stackType @[Operation, [Operation], (StorageSkeleton tokenAddress)]
  cons; pair

setManager
  :: forall tokenAddress. (IsoValue tokenAddress)
  => (forall s1. RuntimeValues s1)
  -> Entrypoint Address (StorageSkeleton tokenAddress)
setManager runtimeValues = do
  -- assert selfIsUpdatingTokenPool == False
  dip (failIfSelfIsUpdatingTokenPool runtimeValues)
  -- assert amount == 0
  failIfAmountIsNotZero runtimeValues
  
  -- assert that the sender is the manager
  duupX @2; stToField #manager; sender; assertEq (senderIsNotContractManager . errorMessages $ runtimeValues)
  stackType @[Address, (StorageSkeleton tokenAddress)]

  stSetField #manager
  nil; pair
  

-- | Allow the baker to send staking rewards to the Dexter contract.
default_
  :: forall tokenAddress. (IsoValue tokenAddress)
  => (forall s1. RuntimeValues s1)
  -> Entrypoint () (StorageSkeleton tokenAddress)
default_ runtimeValues = do  
  drop

  -- assert selfIsUpdatingTokenPool == False
  failIfSelfIsUpdatingTokenPool runtimeValues

  -- dexter is responsible for keeping track of how much XTZ it has
  stGetField #xtzPool; amount; add; stSetField #xtzPool
  nil; pair
