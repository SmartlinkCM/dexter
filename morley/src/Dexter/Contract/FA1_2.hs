{-|
Module      : Dexter.Contract.FA1_2
Copyright   : (c) camlCase, 2020

-}

{-# LANGUAGE DeriveAnyClass            #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE TypeFamilies              #-}

module Dexter.Contract.FA1_2 where

import Lorentz

-- (address :from, (address :to, nat :value))    %transfer
type TransferParams   = (Address, (Address, Natural))

-- (address :owner, contract nat)              %getBalance
type GetBalanceParams = (Address, ContractRef Natural)

data FA1_2
  = Transfer   TransferParams
  | GetBalance GetBalanceParams
  deriving (Generic, IsoValue)

instance ParameterHasEntryPoints FA1_2 where
  type ParameterEntryPointsDerivation FA1_2 = EpdPlain
