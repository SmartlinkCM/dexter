{-|
Module      : Dexter.Contract.XtzToToken
Copyright   : (c) camlCase, 2020

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE FunctionalDependencies    #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Dexter.Contract.XtzToToken
  ( xtzToToken
  , calculateTokensBought
  , TransferDexterTokensTo
  ) where

import qualified Dexter.Contract.FA1_2 as FA1_2
import qualified Dexter.Contract.FA2 as FA2
import Dexter.Contract.Core
import Lorentz

-- stackType type synonyms to improve readability
type Denominator      = Natural
type XtzIn            = Natural
type Numerator        = Natural
type TokensBought     = Natural

-- | An address can sell XTZ to a dexter contract to get FA1.2 token.
-- The rate is determined by the pool sizes. A 0.3% fee is incurred and
-- split evenly amongst the liquidty providers.
xtzToToken
  :: forall updateTokenPoolInternalParams tokenAddress param storage.
     ( IsoValue updateTokenPoolInternalParams
     , HasAnnotation updateTokenPoolInternalParams
     , Parameter updateTokenPoolInternalParams ~ param
     , IsoValue tokenAddress
     , StorageSkeleton tokenAddress ~ storage
     , SelfCallingDefault param storage
     , TransferDexterTokensTo param storage)
  => (forall s1. RuntimeValues s1)
  -> Entrypoint XtzToTokenParams storage
xtzToToken runtimeValues = do
  -- assert that sender is the source
  sender; source; assertEq (senderMustBeTheTransactionSource . errorMessages $ runtimeValues)
  -- assert that the to address is the source
  getField #to; fromNamed #receiver; source; assertEq (toMustBeTheTransactionSource . errorMessages $ runtimeValues)  
  -- assert that selfIsUpdatingTokenPool == False
  dip (failIfSelfIsUpdatingTokenPool runtimeValues)
  -- assert now < deadline
  getField #deadline; now; assertLt (deadlinePassed . errorMessages $ runtimeValues)
  -- assert amount > 0
  failIfAmountIsZero runtimeValues
  -- assert minTokensBought > 0
  getField #minTokensBought; push @Natural 0; assertLt (minTokensBoughtIsZero . errorMessages $ runtimeValues)
  -- assert xtzPool > 0
  dip (do stGetField #xtzPool; push @Mutez zeroMutez; assertLt (xtzPoolIsZero . errorMessages $ runtimeValues))
  -- assert tokenPool > 0
  dip (do stGetField #tokenPool; push @Natural 0; assertLt (tokenPoolIsZero . errorMessages $ runtimeValues))

  calculateTokensBought runtimeValues; stackType @[TokensBought, XtzToTokenParams, (StorageSkeleton tokenAddress)]

  -- storage.tokenPool = tokenPool - tokensBought
  dup; duupX @4; stToField #tokenPool; sub; intToNatural
  dip (dig @2); stSetField #tokenPool
  stackType @[(StorageSkeleton tokenAddress), TokensBought, XtzToTokenParams]

  -- storage.xtzPool = xtzPool + amount
  stGetField #xtzPool; amount; add; stSetField #xtzPool
  stackType @[(StorageSkeleton tokenAddress), TokensBought, XtzToTokenParams]

  -- transfer token from Dexter to the to address
  dip (dip (do toField #to; fromNamed #receiver; selfCallingDefault @param @storage; address; toNamed #owner))
  stackType @[(StorageSkeleton tokenAddress), Natural, Owner, Address]
  transferDexterTokensTo runtimeValues
  dip nil; cons; pair

-- | Calculate the tokens the user has bought based on the tokenPool and xtzPool
-- sizes. The liquidity providers keep 0.3% of the XTZ sent in.
calculateTokensBought ::
  forall s tokenAddress.
  (IsoValue tokenAddress) =>
  (forall s1. RuntimeValues s1) ->
  XtzToTokenParams & (StorageSkeleton tokenAddress) & s :-> TokensBought & XtzToTokenParams & (StorageSkeleton tokenAddress) & s
calculateTokensBought runtimeValues = do
  -- tokensBought = (xtzIn * 997 * tokenPool) / (xtzPool * 1000 + xtzIn * 997)
  -- calculate the denominator
  duupX @2; stToField #xtzPool; mutezToNatural; push @Natural 1000; mul;
  amount; mutezToNatural; dup; dip (do push @Natural 997; mul; add;)
  stackType @(XtzIn : Denominator : XtzToTokenParams : (StorageSkeleton tokenAddress) : _)

  -- calculate the numerator
  push @Natural 997; mul; duupX @4; stToField #tokenPool; mul
  stackType @(Numerator : Denominator : XtzToTokenParams : (StorageSkeleton tokenAddress) : _)

  ediv; ifNone (do push (divByZero . errorMessages $ runtimeValues); failWith) car
  stackType @(TokensBought : XtzToTokenParams : (StorageSkeleton tokenAddress) : _)

  dup; dip (do dip (getField #minTokensBought); assertGe (tokensBoughtLessThanMin . errorMessages $ runtimeValues) )

class TransferDexterTokensTo param storage | param -> storage, storage -> param where
  transferDexterTokensTo :: (forall s1. RuntimeValues s1) -> storage : Natural : Owner : Address : s :-> Operation : storage : s

instance TransferDexterTokensTo ParameterFA1_2 StorageFA1_2 where
  transferDexterTokensTo runtimeValues = do
    -- get FA1.2 address from storage
    stGetField #tokenAddress; contractCalling @FA1_2.FA1_2 (Call @"Transfer")
    ifNone (do push (contractDoesNotHaveTransferEntrypoint . errorMessages $ runtimeValues); failWith)
           nop
    -- send 0 mutez to the FA1.2 contract
    push @Mutez zeroMutez
    stackType @( Mutez : ContractRef FA1_2.TransferParams : StorageFA1_2 : Natural : Owner : Address : _)
    dig @3; dig @5; dig @5; fromNamed #owner; dip pair; pair
    transferTokens

instance TransferDexterTokensTo ParameterFA2 StorageFA2 where
  transferDexterTokensTo runtimeValues = do
    -- get FA1.2 address from storage
    stGetField #tokenAddress; unpair; contractCalling @FA2.FA2 (Call @"Transfer")
    ifNone (do push (contractDoesNotHaveTransferEntrypoint . errorMessages $ runtimeValues); failWith)
           nop    
    -- send 0 mutez to the FA2 contract
    push @Mutez zeroMutez
    stackType @( Mutez : ContractRef [FA2.Transfer] : FA2.TokenId : StorageFA2 : Natural : Owner : Address : _)
    -- [(Address, [(Address, (Natural, Natural))])]
    -- [(from, [(to, (tokenId, amount))])]
    constructT @FA2.TransferDestination
      ( fieldCtor $ duupX @7 >> toNamed #to_
      , fieldCtor $ duupX @3 >> toNamed #token_id
      , fieldCtor $ duupX @5 >> toNamed #amount
      )
    nil; swap; cons

    stackType @( [FA2.TransferDestination] : Mutez : ContractRef [FA2.Transfer] : FA2.TokenId : StorageFA2 : Natural : Owner : Address : _)

    constructT @FA2.Transfer
      ( fieldCtor $ duupX @7 >> fromNamed #owner >> toNamed #from_
      , fieldCtor $ dup >> toNamed #txs
      )
    nil; swap; cons
    dip drop

    stackType @( [FA2.Transfer] : Mutez : ContractRef [FA2.Transfer] : FA2.TokenId : StorageFA2 : Natural : Owner : Address : _)

    transferTokens
    swap; drop
    dip (dip (do drop; drop; drop))
