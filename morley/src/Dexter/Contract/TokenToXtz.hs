{-|
Module      : Dexter.Contract.TokenToXtz
Copyright   : (c) camlCase, 2020

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Dexter.Contract.TokenToXtz
  ( tokenToXtz
  , calculateXtzBought
  ) where

import Dexter.Contract.Core
import Lorentz

type Denominator = Natural
type Numerator   = Natural

-- | An address can sell FA1.2 tokens to a dexter contract to get XTZ.
-- The rate is determined by the pool sizes. A 0.3% fee is incurred and
-- split evenly amongst the liquidty providers.
tokenToXtz
  :: forall updateTokenPoolInternalParams tokenAddress storage param.
     ( IsoValue updateTokenPoolInternalParams
     , Parameter updateTokenPoolInternalParams ~ param
     , IsoValue tokenAddress
     , StorageSkeleton tokenAddress ~ storage
     , TransferOwnerTokensToDexter param storage)
  => (forall s1. RuntimeValues s1)
  -> Entrypoint TokenToXtzParams storage
tokenToXtz runtimeValues = do
  -- assert that sender is the source
  sender; source; assertEq (senderMustBeTheTransactionSource . errorMessages $ runtimeValues)

  -- assert that the owner is the source
  getField #owner; fromNamed #owner; source; assertEq (ownerMustBeTheTransactionSource . errorMessages $ runtimeValues)

  -- assert that the to address is the source
  getField #to; fromNamed #receiver; source; assertEq (toMustBeTheTransactionSource . errorMessages $ runtimeValues)

  -- assert that selfIsUpdatingTokenPool == False
  dip (failIfSelfIsUpdatingTokenPool runtimeValues)
  -- assert now < deadline
  getField #deadline; now; assertLt (deadlinePassed . errorMessages $ runtimeValues)  
  -- assert amount == 0
  failIfAmountIsNotZero runtimeValues
  -- assert xtzPool > 0
  dip (do stGetField #xtzPool; push @Mutez zeroMutez; assertLt (xtzPoolIsZero . errorMessages $ runtimeValues))
  -- assert tokenPool > 0
  dip (do stGetField #tokenPool; push @Natural 0; assertLt (tokenPoolIsZero . errorMessages $ runtimeValues))
  -- assert tokensSold > 0
  getField #tokensSold; push @Natural 0; assertLt (tokensSoldIsZero . errorMessages $ runtimeValues)    
  -- assert minTokensBought > 0
  getField #minXtzBought; push @Mutez zeroMutez; assertLt (minXtzBoughtIsZero . errorMessages $ runtimeValues)

  calculateXtzBought runtimeValues

  -- update tokenPool
  -- storage.tokenPool  = storage.tokenPool + tokensSold
  dip (do swap; stGetField #tokenPool; duupX @3; toField #tokensSold; add; stSetField #tokenPool)
  stackType @[XtzBought, storage, TokenToXtzParams]

  -- update xtzPool
  dup; dip (do dip (stGetField #xtzPool); fromNamed #xtzBought; swap; sub; stSetField #xtzPool)
  stackType @[XtzBought, storage, TokenToXtzParams]

  -- send xtzBought
  duupX @3; toField #to; fromNamed #receiver; Lorentz.contract @()
  ifNone (do push (contractDoesNotHaveDefaultEntrypoint . errorMessages $ runtimeValues); failWith)
         nop
  dig @1; fromNamed #xtzBought; push (); transferTokens
  stackType @[Operation, storage, TokenToXtzParams]

  -- transfer tokens to dexter
  dip (do dip (do getField #tokensSold;
                  dip (toField #owner)
              )
          transferOwnerTokensToDexter runtimeValues)
  stackType @[Operation, Operation, storage]

  -- the first operation is from transferring XTZ, the second from transferring FA1.2
  -- cons prepends a value so we need to cons the second operation to nil first, the then
  -- the first operation
  nil; dig @2; cons; swap; cons; pair

calculateXtzBought
  :: forall s tokenAddress. (IsoValue tokenAddress)     
  => (forall s1. RuntimeValues s1)
  -> TokenToXtzParams & StorageSkeleton tokenAddress & s
  :-> XtzBought & TokenToXtzParams & StorageSkeleton tokenAddress & s
calculateXtzBought runtimeValues = do
  -- xtzBought = (tokens_sold * 997 * xtz_pool) / (storage.s.token_pool * 1000 + (tokens_sold * 997))
  getField #tokensSold; push @Natural 997; mul
  duupX @3; stToField #tokenPool; push @Natural 1000; mul; add
  stackType @(Denominator : TokenToXtzParams : StorageSkeleton tokenAddress : _)

  duupX @2; toField #tokensSold; push @Natural 997; mul
  duupX @4; stToField #xtzPool; mutezToNatural; mul
  stackType @(Numerator : Denominator : TokenToXtzParams : StorageSkeleton tokenAddress : _)

  ediv; ifNone (do push (divByZero . errorMessages $ runtimeValues); failWith) car; naturalToMutez

  dup; dip (do dip (getField #minXtzBought); assertGe (xtzBoughtLessThanMin . errorMessages $ runtimeValues) )
  toNamed #xtzBought
