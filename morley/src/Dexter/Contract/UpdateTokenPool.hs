{-|
Module      : Dexter.Contract.UpdateTokenPool
Copyright   : (c) camlCase, 2020

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE FunctionalDependencies    #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE UndecidableInstances      #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Dexter.Contract.UpdateTokenPool
  ( updateTokenPool
  , updateTokenPoolInternal
  , AssertSenderIsTokenAddress
  , CallUpdateTokenPoolInternal
  , SetFieldTokenPool  
  ) where

import qualified Dexter.Contract.FA1_2 as FA1_2
import qualified Dexter.Contract.FA2   as FA2

import Data.Typeable
import Dexter.Contract.Core
import Lorentz

-- | The token pool value in Dexter may become out of sync with its true value
-- in FA1.2. Originally we tried to integrate a call to getBalance everewhere we
-- needed it, but it caused the gas costs to increase excessively. For security
-- purposes, updateTokenPool sets the selfIsUpdatingTokenPool flag to True, then calls
-- FA1.2 to get the balance and calls updateTokenPoolInternal with the balance.
-- If selfIsUpdatingTokenPool is True, then updateTokenPoolInternal will succesfully
-- update tokenPool, otherwise it will fail. This is to prevent attackers from
-- calling other entyrpoints in FA1.2 that have the same type signature as
-- getBalance and try to alter the tokenPool value in ways that were not intended.

updateTokenPool
  :: forall updateTokenPoolInternalParams tokenAddress param storage.
     ( IsoValue updateTokenPoolInternalParams
     , HasAnnotation updateTokenPoolInternalParams
     , Typeable updateTokenPoolInternalParams
     , Parameter updateTokenPoolInternalParams ~ param
     , IsoValue tokenAddress
     , Typeable tokenAddress
     , StorageSkeleton tokenAddress ~ storage
     , CallUpdateTokenPoolInternal param storage)
  => (forall s1. RuntimeValues s1)
  -> Entrypoint KeyHash storage
updateTokenPool runtimeValues = do
  -- assert amount == 0
  failIfAmountIsNotZero runtimeValues

  -- assert that the KeyHash is an implicit acccount that matches the sender's
  -- address
  implicitAccount; address; sender; assertEq (unsafeUpdateTokenPool . errorMessages $ runtimeValues)

  -- assert that selfIsUpdatingTokenPool == False
  failIfSelfIsUpdatingTokenPool runtimeValues
  stackType @(storage : _)

  push @Bool True; stSetField #selfIsUpdatingTokenPool  
  callUpdateTokenPoolInternal runtimeValues
  stackType @(Operation : storage : _)
  dip nil; cons; pair

class CallUpdateTokenPoolInternal param storage | param -> storage, storage -> param where
  callUpdateTokenPoolInternal :: (forall s1. RuntimeValues s1) -> storage : s :-> Operation : storage : s

instance CallUpdateTokenPoolInternal ParameterFA1_2 StorageFA1_2 where
  callUpdateTokenPoolInternal runtimeValues = do
    stGetField #tokenAddress; contractCalling @FA1_2.FA1_2 (Call @"GetBalance")
    ifNone (do push (contractDoesNotHaveGetBalanceEntrypoint . errorMessages $ runtimeValues); failWith)
           nop  
    push @Mutez zeroMutez
  
    selfCalling @ParameterFA1_2 (Call @"UpdateTokenPoolInternal")
    selfCalling @ParameterFA1_2 CallDefault; address; pair

    transferTokens

instance CallUpdateTokenPoolInternal ParameterFA2 StorageFA2 where
  callUpdateTokenPoolInternal runtimeValues = do
    -- get FA2 BalanceOf entrypoint
    stGetField #tokenAddress; unpair
    contractCalling @FA2.FA2 (Call @"Balance_of")
    ifNone (do push (contractDoesNotHaveBalanceOfEntrypoint . errorMessages $ runtimeValues); failWith)
           nop      
    stackType @(ContractRef FA2.BalanceOfParam : FA2.TokenId : StorageFA2 : _)

    -- prepare to send 0 mutez
    push @Mutez zeroMutez
    stackType @(Mutez : ContractRef FA2.BalanceOfParam : FA2.TokenId : StorageFA2 : _)
    
    -- get the Dexter UpdateTokenPoolInternal entrypoint
    selfCalling @ParameterFA2 (Call @"UpdateTokenPoolInternal")
    stackType @(ContractRef [FA2.BalanceOfResponse] : Mutez : ContractRef FA2.BalanceOfParam : FA2.TokenId : StorageFA2 : _)

    -- -- make BalanceOfRequest    
    constructT @FA2.BalanceOfRequest
      ( fieldCtor $ selfCalling @ParameterFA2 CallDefault >> address >> toNamed #owner
      , fieldCtor $ duupX @4 >> toNamed #token_id
      )

    nil; swap; cons

    -- make BalanceOfParam
    constructT @FA2.BalanceOfParam
      ( fieldCtor $ dup >> toNamed #requests
      , fieldCtor $ duupX @2 >> toNamed #callback
      )    

    -- clean up
    swap; drop; swap; drop
    -- send request
    transferTokens
    -- clean up
    swap; drop

updateTokenPoolInternal
  :: forall updateTokenPoolInternalParams tokenAddress param storage.
     ( IsoValue updateTokenPoolInternalParams
     , HasAnnotation updateTokenPoolInternalParams
     , Typeable updateTokenPoolInternalParams
     , Parameter updateTokenPoolInternalParams ~ param
     , IsoValue tokenAddress
     , Typeable tokenAddress
     , StorageSkeleton tokenAddress ~ storage
     , AssertSenderIsTokenAddress updateTokenPoolInternalParams storage
     , SetFieldTokenPool updateTokenPoolInternalParams storage)
  => (forall s1. RuntimeValues s1)
  -> Entrypoint updateTokenPoolInternalParams storage
updateTokenPoolInternal runtimeValues = do
  failIfAmountIsNotZero runtimeValues
  
  -- assert that selfIsUpdatingTokenPool is true, this is to prevent attacks being
  -- called directly from the token contract
  duupX @2; stToField #selfIsUpdatingTokenPool; assert (notSelfIsUpdatingTokenPool . errorMessages $ runtimeValues)
  
  -- assert that the sender is the tokenAddress
  assertSenderIsTokenAddress runtimeValues

  -- set the tokenPool
  setFieldTokenPool runtimeValues

  -- set selfIsUpdatingTokenPool to false
  push @Bool False; stSetField #selfIsUpdatingTokenPool
  nil; pair
  
class AssertSenderIsTokenAddress param storage | param -> storage, storage -> param where
  assertSenderIsTokenAddress :: (forall s1. RuntimeValues s1) -> param : storage : t :-> param : storage : t

instance AssertSenderIsTokenAddress UpdateTokenPoolInternalParamsFA1_2 StorageFA1_2 where
  assertSenderIsTokenAddress runtimeValues = do  
    -- assert that the sender is the tokenAddress
    duupX @2; stToField #tokenAddress; sender
    assertEq (notCalledFromToken . errorMessages $ runtimeValues)

instance AssertSenderIsTokenAddress UpdateTokenPoolInternalParamsFA2 StorageFA2 where
  assertSenderIsTokenAddress runtimeValues = do  
    -- assert that the sender is the tokenAddress
    duupX @2; stToField #tokenAddress; car; sender
    assertEq (notCalledFromToken . errorMessages $ runtimeValues)


class SetFieldTokenPool updateTokenPoolInternal storage | updateTokenPoolInternal -> storage, storage -> updateTokenPoolInternal where
  setFieldTokenPool :: (forall s1. RuntimeValues s1) -> updateTokenPoolInternal : storage : s :-> storage : s

instance SetFieldTokenPool UpdateTokenPoolInternalParamsFA1_2 StorageFA1_2 where
  setFieldTokenPool _ = do  
    stSetField #tokenPool

instance SetFieldTokenPool UpdateTokenPoolInternalParamsFA2 StorageFA2 where
  setFieldTokenPool runtimeValues = do
    stackType @([FA2.BalanceOfResponse] : StorageFA2 : _)
    -- FA2 returns a list, we only query for the tokenId stored in dexter storage
    -- it should call with a list with one item, otherwise this is unexpected behavior
    ifCons
      (do -- ((Address, TokenId), Balance)
          stackType @(FA2.BalanceOfResponse : [FA2.BalanceOfResponse] : StorageFA2 : _)
          -- drop the rest of the list
          swap; drop
          stackType @(FA2.BalanceOfResponse : StorageFA2 : _)

          -- assert that the token_id from BalanceOfResponse and storage match
          dup; toField #request; toField #token_id
          duupX @3; stToField #tokenAddress; cdr;
          assertEq (tokenIdDoesntMatch . errorMessages $ runtimeValues)

          -- assert that the owner from BalanceOfResponse and the address of this contract match
          dup; toField #request; toField #owner
          selfCalling @ParameterFA1_2 CallDefault; address
          assertEq (ownerDoesntMatch . errorMessages $ runtimeValues)

          -- set Dexter's FA2 balance as the balance from BalanceOfResponse
          toField #balance
          stSetField #tokenPool          
      )
      (do push (fa2NoBalance . errorMessages $ runtimeValues); failWith)
