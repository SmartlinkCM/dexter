{-|
Module      : Dexter.Contract.RemoveLiquidity
Copyright   : (c) camlCase, 2020

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE FunctionalDependencies    #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Dexter.Contract.RemoveLiquidity where

import Data.Typeable (Typeable)
import qualified Dexter.Contract.FA1_2 as FA1_2
import qualified Dexter.Contract.FA2 as FA2
import Dexter.Contract.Core
import Lorentz

type XtzWithdrawn    = Mutez
type LqtBalance      = Natural
type NewLqtBalance   = Natural
type TokensWithdrawn = Natural

-- | An address redeem liquidity it has provided. The exchange rate of XTZ to
-- Token fluctuates with the ratio of xtzPool to tokenPool. The owner of
-- liquidty can burn its internal dexter liquidy tokens to redeem an equivalent
-- amount of XTZ and Token.
removeLiquidity
  :: forall updateTokenPoolInternalParams tokenAddress param storage.
     ( IsoValue updateTokenPoolInternalParams
     , Parameter updateTokenPoolInternalParams ~ param
     , Typeable tokenAddress     
     , IsoValue tokenAddress
     , StorageSkeleton tokenAddress ~ storage
     , TransferDexterTokensToReceiver param storage
     )
  => (forall s1. RuntimeValues s1)
  -> Entrypoint RemoveLiquidityParams storage
removeLiquidity runtimeValues = do
  -- assert that sender is the source
  sender; source; assertEq (senderMustBeTheTransactionSource . errorMessages $ runtimeValues)

  -- assert that the owner is the source
  getField #owner; fromNamed #owner; source; assertEq (ownerMustBeTheTransactionSource . errorMessages $ runtimeValues)

  -- assert that the to address is the source
  getField #to; fromNamed #receiver; source; assertEq (toMustBeTheTransactionSource . errorMessages $ runtimeValues)
  
  -- assert that selfIsUpdatingTokenPool == False
  dip (failIfSelfIsUpdatingTokenPool runtimeValues)
  
  removeLiquidityAndPrepOperations runtimeValues
  -- run operations

  -- send xtz
  duupX @4; toField #to; fromNamed #receiver; Lorentz.contract @()
  ifNone (do push (contractDoesNotHaveDefaultEntrypoint . errorMessages $ runtimeValues); failWith)
         nop
  dig @3; push (); transferTokens

  -- nil; swap; cons
  stackType @[Operation, storage, TokensWithdrawn, RemoveLiquidityParams]
  
  -- transfer tokens
  dip (do dip (dip (toField #to)); transferDexterTokensToReceiver runtimeValues);
  stackType @[Operation, Operation, storage]
  nil; swap; cons; swap; cons; pair

-- | removeLiquidity without operations
removeLiquidityAndPrepOperations
  :: forall s tokenAddress. (IsoValue tokenAddress)
  => (forall s1. RuntimeValues s1)
  -> RemoveLiquidityParams : (StorageSkeleton tokenAddress) : s
  :-> (StorageSkeleton tokenAddress) : TokensWithdrawn : XtzWithdrawn : RemoveLiquidityParams : s
removeLiquidityAndPrepOperations runtimeValues = do
  removeLiquidityInitialAsserts runtimeValues

  getOwnerBalanceCheckAndUpdateSenderAllowance runtimeValues
  stackType @(LqtBalance : RemoveLiquidityParams : (StorageSkeleton tokenAddress) :_)

  -- xtzWithdrawn = lqtBurned * xtzPool / lqtTotal
  duupX @2; toField #lqtBurned; duupX @4; stToField #xtzPool; mutezToNatural; mul;
  dip (do duupX @3; stToField #lqtTotal); ediv; ifNone (do push (divByZero . errorMessages $ runtimeValues); failWith) car; naturalToMutez
  stackType @(XtzWithdrawn : LqtBalance : RemoveLiquidityParams : (StorageSkeleton tokenAddress): _)

  -- xtzWithdrawn >= minXtzWithdrawn
  dup; duupX @4; toField #minXtzWithdrawn; assertLe (xtzWithdrawnIsLessThanMin . errorMessages $ runtimeValues)
  stackType @(XtzWithdrawn : LqtBalance : RemoveLiquidityParams : (StorageSkeleton tokenAddress): _)

  -- tokensWithdrawn = lqtBurned * tokenPool / lqtTotal
  duupX @3; toField #lqtBurned; dip (do duupX @4; stGetField #lqtTotal; swap; stToField #tokenPool);
  mul; ediv; ifNone (do push (divByZero . errorMessages $ runtimeValues); failWith) car
  stackType @(TokensWithdrawn : XtzWithdrawn : LqtBalance : RemoveLiquidityParams : (StorageSkeleton tokenAddress): _)

  -- tokenWithdrawn >= minTokensWithdrawn
  dup; duupX @5; toField #minTokensWithdrawn; assertLe (minTokensWithdrawLessThanMin . errorMessages $ runtimeValues)
  stackType @(TokensWithdrawn : XtzWithdrawn : LqtBalance : RemoveLiquidityParams : (StorageSkeleton tokenAddress): _)

  -- NewBalance = LqtBalance - LqtBurned
  duupX @4; toField #lqtBurned; dig @3; sub; intToNatural
  stackType @(NewLqtBalance : TokensWithdrawn : XtzWithdrawn : RemoveLiquidityParams : (StorageSkeleton tokenAddress): _)

  -- set owner's newBalance
  dig @4; dup; duupX @6; toField #owner; toAccount
  stackType @(Account : (StorageSkeleton tokenAddress) : NewLqtBalance : TokensWithdrawn : XtzWithdrawn : RemoveLiquidityParams : _)
  dig @2; setField #balance;
  stackType @(Account : (StorageSkeleton tokenAddress) : TokensWithdrawn : XtzWithdrawn : RemoveLiquidityParams : _)
  dip (getField #accounts); some; duupX @6; toField #owner; fromNamed #owner; update; setField #accounts
  stackType @((StorageSkeleton tokenAddress) : TokensWithdrawn : XtzWithdrawn : RemoveLiquidityParams: _)
  
  -- newLqtTotal = lqtTotal - LqtBurned
  stGetField #lqtTotal; duupX @5; toField #lqtBurned; swap; sub; intToNatural; stSetField #lqtTotal
  stackType @((StorageSkeleton tokenAddress) : TokensWithdrawn : XtzWithdrawn : RemoveLiquidityParams : _)

  -- newTokenPool = tokenPool - tokensWithdrawn
  stGetField #tokenPool; duupX @3; swap; sub; intToNatural; stSetField #tokenPool
  
  -- storage.xtzPool = storage.xtzPool - xtzWithdrawn
  stGetField #xtzPool; duupX @4; swap; sub; stSetField #xtzPool

-- | Initial asserts for removeLiquidity
removeLiquidityInitialAsserts
  :: forall s tokenAddress.
  (IsoValue tokenAddress) =>
  (forall s1. RuntimeValues s1)
  -> RemoveLiquidityParams : (StorageSkeleton tokenAddress) : s :-> RemoveLiquidityParams : (StorageSkeleton tokenAddress) : s
removeLiquidityInitialAsserts runtimeValues = do
  -- now < deadline
  getField #deadline; now; assertLt (deadlinePassed . errorMessages $ runtimeValues)
  -- amount == 0
  failIfAmountIsNotZero runtimeValues  
  -- minXtzWithdrawn > 0
  getField #minXtzWithdrawn; push @Mutez zeroMutez; assertLt (minXtzWithdrawnIsZero . errorMessages $ runtimeValues)
  -- minTokensWithdrawn > 0
  getField #minTokensWithdrawn; push @Natural 0; assertLt (minTokensWithdrawnIsZero . errorMessages $ runtimeValues)
  -- lqtBurned > 0
  getField #lqtBurned; push @Natural 0; assertLt (lqtBurnedIsZero . errorMessages $ runtimeValues)
    
getOwnerBalanceCheckAndUpdateSenderAllowance
  :: forall s tokenAddress.
  (IsoValue tokenAddress) =>
  (forall s1. RuntimeValues s1)
  -> RemoveLiquidityParams : (StorageSkeleton tokenAddress) : s :-> Natural : RemoveLiquidityParams : (StorageSkeleton tokenAddress) : s
getOwnerBalanceCheckAndUpdateSenderAllowance runtimeValues = do
  getField #owner; fromNamed #owner; dip (do duupX @2; toField #accounts); get
  ifNone (do push (ownerHasNoLiquidity . errorMessages $ runtimeValues); failWith) nop;
  stackType @(Account : RemoveLiquidityParams : (StorageSkeleton tokenAddress) : _)
 
  -- assert that lqtBurned does not exceed the owner's balance
  swap; getField #lqtBurned; duupX @3; toField #balance; assertGe (lqtBurnedIsGreaterThanOwnersBalance . errorMessages $ runtimeValues)
  stackType @(RemoveLiquidityParams : Account : (StorageSkeleton tokenAddress) : _)

  -- check if the sender is the owner
  getField #owner; fromNamed #owner; sender; eq;
  if_
    (do -- sender is owner, get the their liquidity balance
        stackType @(RemoveLiquidityParams : Account : (StorageSkeleton tokenAddress) : _)
        swap; toField #balance
    )
    (do -- the sender is not the owner
        -- get their allowance
        stackType @(RemoveLiquidityParams : Account : (StorageSkeleton tokenAddress) : _)
        swap; getField #approvals; sender; get;
        ifNone (do push (senderHasNoApprovalBalance . errorMessages $ runtimeValues); failWith) nop;
        -- approved amount for sender is on the top
        stackType @(Natural : Account : RemoveLiquidityParams : (StorageSkeleton tokenAddress) : _)
        -- check that allowances is greater than or equal to lqtBurned;
        dip (do swap; getField #lqtBurned; dip (push @Integer 0)); sub; dup
        stackType @(Integer : Integer : Integer : RemoveLiquidityParams : Account : (StorageSkeleton tokenAddress) : _)        
        dip (assertGe (senderApprovalBalanceIsLessThanLqtBurned . errorMessages $ runtimeValues)); abs
        stackType @(Natural : RemoveLiquidityParams : Account : (StorageSkeleton tokenAddress) : _)

        -- update sender amount in approval to their balance minus liquidity burned
        some; dig @2; getField #approvals; dig @2; sender; update; setField #approvals
        stackType @(Account : RemoveLiquidityParams : (StorageSkeleton tokenAddress) : _)

        getField #balance; dip (do some; dug @1; getField #owner; fromNamed #owner; swap)
        stackType @(Natural : RemoveLiquidityParams : Address : Maybe Account : (StorageSkeleton tokenAddress) : _)
        dip (dip (do dip (dip (getField #accounts)); update; setField #accounts))
    )

class TransferDexterTokensToReceiver param storage | param -> storage, storage -> param where
  transferDexterTokensToReceiver :: (forall s1. RuntimeValues s1) -> storage : Natural : Receiver : s :-> Operation : storage : s

-- | Transfer FA1.2 tokens from Dexter to a Receiver.
instance TransferDexterTokensToReceiver ParameterFA1_2 StorageFA1_2 where
  transferDexterTokensToReceiver runtimeValues = do
    -- get FA1.2 address from storage
    stGetField #tokenAddress; contractCalling @FA1_2.FA1_2 (Call @"Transfer")
    ifNone (do push (contractDoesNotHaveTransferEntrypoint . errorMessages $ runtimeValues); failWith)
           nop
    -- send 0 mutez to the FA1.2 contract
    push @Mutez zeroMutez
    stackType @( Mutez : ContractRef FA1_2.TransferParams : StorageFA1_2 : Natural : Receiver : _)
    -- (from, (to, value))
    dig @3; dig @4; fromNamed #receiver; pair; selfCalling @ParameterFA1_2 CallDefault; address; pair
    transferTokens

-- | Transfer FA2 tokens from Dexter to a Receiver.
instance TransferDexterTokensToReceiver ParameterFA2 StorageFA2 where
  transferDexterTokensToReceiver runtimeValues = do
    -- get FA1.2 address from storage
    stGetField #tokenAddress; unpair; contractCalling @FA2.FA2 (Call @"Transfer")
    ifNone (do push (contractDoesNotHaveTransferEntrypoint . errorMessages $ runtimeValues); failWith)
           nop
    -- send 0 mutez to the FA1.2 contract
    push @Mutez zeroMutez
    stackType @( Mutez : ContractRef [FA2.Transfer] : FA2.TokenId : StorageFA2 : Natural : Receiver : _)

    -- prepare the transfer values
    -- [(Address, [(Address, (Natural, Natural))])]
    -- [(from, [(to, (tokenId, amount))])]
    constructT @FA2.TransferDestination
      ( fieldCtor $ duupX @6 >> fromNamed #receiver >> toNamed #to_
      , fieldCtor $ duupX @3 >> toNamed #token_id
      , fieldCtor $ duupX @5 >> toNamed #amount
      )
    nil; swap; cons

    stackType @( [FA2.TransferDestination] : Mutez : ContractRef [FA2.Transfer] : FA2.TokenId : StorageFA2 : Natural : Receiver : _)
    
    constructT @FA2.Transfer
      ( fieldCtor $ selfCalling @ParameterFA2 CallDefault >> address >> toNamed #from_
      , fieldCtor $ dup >> toNamed #txs
      )
    nil; swap; cons
    dip drop

    stackType @( [FA2.Transfer] : Mutez : ContractRef [FA2.Transfer] : FA2.TokenId : StorageFA2 : Natural : Receiver : _)
    transferTokens
    dip (do drop; dip (do drop; drop))
