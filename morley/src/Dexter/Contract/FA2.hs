{-|
Module      : Dexter.Contract.FA2
Copyright   : (c) camlCase, 2020

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DeriveAnyClass            #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}

module Dexter.Contract.FA2 where

import Lorentz

-- =============================================================================
-- Transfer
-- =============================================================================

type TokenId =
  ( "token_id" :! Natural
  )

type TransferDestination =
  ( "to_"      :! Address
  , "token_id" :! TokenId
  , "amount"   :! Natural
  )

type Transfer =
  ( "from_" :! Address
  , "txs"   :! [TransferDestination]
  )

-- =============================================================================
-- Balance_of
-- =============================================================================

type BalanceOfRequest =
  ( "owner"    :! Address
  , "token_id" :! TokenId
  )

type BalanceOfResponse =
  ( "request" :! BalanceOfRequest
  , "balance" :! Natural
  )

type BalanceOfParam =
  ( "requests" :! [BalanceOfRequest]
  , "callback" :! ContractRef [BalanceOfResponse]
  )

-- =============================================================================
-- Entrypoint
-- =============================================================================

data FA2
  = Transfer  [Transfer]
  | Balance_of BalanceOfParam
  deriving (Generic, IsoValue)

instance ParameterHasEntryPoints FA2 where
  type ParameterEntryPointsDerivation FA2 = EpdPlain
