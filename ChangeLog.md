# Changelog for dexter

# Version 1.1 of the dexter FA1.2 contract -- 2021-02-28
* Update the informal specification for changes.
* Include the updated dexter contract with the vulnerability fix from Nomadic Labs.
